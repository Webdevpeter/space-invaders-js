var screen, input, frames, spFrame, lvFrame;
var alSprite, taSprite, ciSprite;
var aliens, dir, tank, bullets, cities;

function main() {
	screen = new Screen(504, 600);
	input = new InputHandler();

	var img = new Image();
	img.addEventListener("load", function(){
		alSprite = [
			[new Sprite(this, 0, 0, 22, 16), new Sprite(this, 0, 16, 22, 16)],
			[new Sprite(this, 22, 0, 16, 16), new Sprite(this, 22, 16, 16, 16)],
			[new Sprite(this, 38, 0, 24, 16), new Sprite(this, 38, 16, 24, 16)]
		];
		taSprite = new Sprite(this, 62, 0, 22, 16);
		ciSprite = new Sprite(this, 84, 8, 36, 24);

		init();
		run();
	});
	img.src = './invaders.png';
};

function init(lives = 3, score = 0) {
	frames = 0;
	spFrame = 0;
	lvFrame = 20;
	dir = 1;

	tank = {
		sprite: taSprite,
		x: (screen.width - taSprite.w) / 2,
		y: screen.height - (30 + taSprite.h),
		lives: lives,
		score: score
	};

	bullets = [];
	cities = {
		canvas: null,
		y: tank.y - (30 + ciSprite.h),
		h: ciSprite.h,

		init: function() {
			this.canvas = document.createElement("canvas");
			this.canvas.width = screen.width;
			this.canvas.height = this.h;
			this.ctx = this.canvas.getContext("2d");

			for(var i = 0; i < 4; i++) {
				this.ctx.drawImage(ciSprite.img, ciSprite.x, ciSprite.y, ciSprite.w, ciSprite.h,
					68 + 111*i, 0, ciSprite.w, ciSprite.h);
			}
		},
		generateDamage: function(x, y) {
			x = Math.floor(x/2) * 2;
			y = Math.floor(y/2) * 2;

			this.ctx.clearRect(x-2, y-2, 4, 4);
			this.ctx.clearRect(x+2, y-4, 2, 4);
			this.ctx.clearRect(x+4, y, 2, 2);
			this.ctx.clearRect(x+2, y+2, 2, 2);
			this.ctx.clearRect(x-4, y+2, 2, 2);
			this.ctx.clearRect(x-6, y, 2, 2);
			this.ctx.clearRect(x-4, y-4, 2, 2);
			this.ctx.clearRect(x-2, y-6, 2, 2);
		},
		hits: function(x, y) {
			y -= this.y;
			var data = this.ctx.getImageData(x, y, 1, 1);
			if(data.data[3] !== 0) {
				this.generateDamage(x, y);
				return true;
			}
			return false;
		}
	}
	cities.init();

	aliens = [];
	var rows = [1, 0, 0, 2, 2];
	for(var i = 0, len = rows.length; i < len; i++) {
		for(var j = 0; j < 10; j++) {
			var a = rows[i];
			aliens.push({
				sprite: alSprite[a],
				x: 30 + j*30 + [0, 4, 0][a],
				y: 30 + i*30,
				w: alSprite[a][0].w,
				h: alSprite[a][0].h
			});
		}
	}
};

function printInfo(livesModifier = 0) {
	screen.ctx.font = '15px serif';
	screen.ctx.fillStyle = '#fff';
	screen.ctx.fillText('Lives: '+(tank.lives + livesModifier), 10, screen.height - 10);
	screen.ctx.fillText('Score: '+ tank.score, 10, 20);
}

function run() {
	var loop = function() {
		update();
		render();

		if(tank.lives > 0 && aliens.length > 0) {
			printInfo(-1);
			window.requestAnimationFrame(loop, screen.canvas);
		} else {
			if(aliens.length === 0) {
				init(tank.lives, tank.score);
				run();
			} else {
				printInfo();
				console.log('Game Over!');
				if(confirm('Game Over! Play again?')) {
					init();
					run();
				}
			}
		}
	}
	window.requestAnimationFrame(loop, screen.canvas);		
};

function update() {

	if(input.isDown(37)) { // Arrow left
		tank.x -= 4;
	}
	if(input.isDown(39)) { // Arrow right
		tank.x += 4;
	}
	tank.x = Math.max(Math.min(tank.x, screen.width - (30 + taSprite.w)), 30);

	if(input.isPressed(32)) { // Space
		bullets.push(new Bullet(tank.x + 10, tank.y, -8, 2, 6, '#fff'));
	}


	outer: for(var i = 0, len = bullets.length; i < len; i++) {
		var b = bullets[i];

		b.update();

		// Bullet vs Bullet collision check
		if(bullets.length >= 2) {
			var slice1start = i===0?1:0;
			var slice1end = i===0?2:i;
			var firstChunk = bullets.slice(slice1start, slice1end);
			var slice2start = i===0?2:i+1;
			var secondChunk = bullets.slice(slice2start, bullets.length-1);

			for(var i2 = 0, lenB = bullets.length; i2 < lenB; i2++) {
				var b2 = bullets[i2];

				if((i !== i2) && AABBIntersect(b.x, b.y, b.width, b.height, b2.x, b2.y, b2.width, b2.height)) {
					bullets.splice(i, 1);
					bullets.splice(i2>i?i2-1:i2, 1);
					i--;
					i--;
					len--;
					len--;
					break outer;
				}
			}

		}

		// Bullet fly out the screen check
		if(b.y + b.height < 0 || b.y > screen.height) {
			bullets.splice(i, 1);
			i--;
			len--;
			continue;
		}

		// Bullet vs Player collision check
		if(AABBIntersect(tank.x, tank.y, tank.sprite.w, tank.sprite.h, b.x, b.y, b.width, b.height)) {
			tank.lives = tank.lives ? tank.lives - 1 : tank.lives;
			bullets.splice(i, 1);
			i--;
			len--;
			continue;
		}

		// Bullets vs Cities collision check
		var h2 = b.height * 0.5;
		if(cities.y < b.y+h2 && b.y+h2 < cities.y + cities.h) {
			if(cities.hits(b.x, b.y+h2)) {
				bullets.splice(i, 1);
				i--;
				len--;
				continue;
			}
		}

		// Bullets vs Aliens collision check
		for(var j = 0, lenB = aliens.length; j < lenB; j++) {
			var a = aliens[j];
			if(AABBIntersect(b.x, b.y, b.width, b.height, a.x, a.y, a.w, a.h) && b.velY < 0) {
				aliens.splice(j, 1);
				tank.score+=10;
				j--;
				lenB--;
				bullets.splice(i, 1);
				i--;
				len--;
				
				switch(lenB) {
					case 30: {
						this.lvFrame = 40;
						break;
					}
					case 10: {
						this.lvFrame = 20;
						break;
					}
					case 5: {
						this.lvFrame = 15;
						break;
					}
					case 1: {
						this.lvFrame = 6;
						break;
					}
				}
			}
		}
	}

	if(Math.random() < 0.03 && aliens.length > 0) {
		var a = aliens[Math.round(Math.random() * (aliens.length - 1))];

		for(var i = 0, len = aliens.length; i < len; i++) {
			var b = aliens[i];

			if(AABBIntersect(a.x, a.y, a.w, 100, b.x, b.y, b.w, b.h)) {
				a = b;
			}
		}
		bullets.push(new Bullet(a.x + a.w*0.5, a.y + a.h, 4, 2, 4, '#fff'));
	}

	frames++;
	if( frames % lvFrame === 0) {
		spFrame = (spFrame + 1) % 2;

		var _max = 0, _min = screen.width;
		for(var i = 0, len = aliens.length; i < len; i++) {
			var a = aliens[i];
			a.x += 30 * dir;

			_max = Math.max(_max, a.x + a.w);
			_min = Math.min(_min, a.x);
		}
		if(_max > screen.width - 30 || _min < 30) {
			dir *= -1;
			for(var i = 0, len = aliens.length; i < len; i++) {
				aliens[i].x += 30 * dir;
				aliens[i].y += 30;
				if(aliens[i].y >= cities.y - 15) {
					tank.lives = 0;
				}
			}
		}
	}
};

function render() {
	screen.clear();
	for(var i = 0, len = aliens.length; i < len; i++) {
		var a = aliens[i];
		screen.drawSprite(a.sprite[spFrame], a.x, a.y);
	}

	screen.ctx.save();
	for(var i = 0, len = bullets.length; i < len; i++) {
		screen.drawBullet(bullets[i]);
	}
	screen.ctx.restore();

	screen.ctx.drawImage(cities.canvas, 0, cities.y);

	screen.drawSprite(tank.sprite, tank.x, tank.y);
};

main();